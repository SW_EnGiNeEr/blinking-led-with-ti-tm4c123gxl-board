//defining a macro and dereferenceing pointer to RCGCGPIO, you can define the name of the macro as what ever you want
// For example, you can name the macro #define Voltage_Pin ................
#define RCGCGPIO (*((unsigned int *)0x400FE608U))

//defining macros and dereferenceing pointers
#define GPIOF_BASE 0x40025000U
#define GPIOF_DIR  (*((unsigned int *)(GPIOF_BASE + 0x400U))) // can also write (GPIOF_BASE + 0x400U)instead of entire address
#define GPIOF_DEN  (*((unsigned int *)0x4002551CU)) // or write entire address instead of (GPIOF_BASE + 0x51CU)
#define GPIOF_DATA (*((unsigned int *)0x400253FCU))

int main() {
   RCGCGPIO  = 0x20U; //clock gating system register, enable clock for GPIOF register, hex 20 for bit 5
   GPIOF_DIR = 0x0EU; //GPIOF pin register, set pins to bit value 1110, so each pin receives 1 for input (0x0EU)
   GPIOF_DEN = 0x0EU; //Digital function register, enable digital signal at register
   
   // These while loops blink the LEDs Red, Blue, then Green, in that order for a millisecond each, the loop repeats
   
   while (1){
      GPIOF_DATA = 0x02U; // GPIOF data register for colors, hex value 2 for red LED
      int counter = 0;
      while (counter < 1000000){ // delay loop
        ++counter;
      }
      GPIOF_DATA = 0x00U; // GPIOF data register, hex value 0 for no input to LED
      counter = 0;
      while (counter < 1000000) { // delay loop
        ++counter;
      }
        GPIOF_DATA = 0x04U; // hex value 4 for blue
        counter = 0;
      while (counter < 1000000){ // delay loop
        ++counter;
      }
       GPIOF_DATA = 0x00U; // GPIOF data register, hex value 0 for no input to LED
      counter = 0;
      while (counter < 1000000) { // delay loop
        ++counter;
      }
      GPIOF_DATA = 0x08U; // hex value 8 for green
        counter = 0;
      while (counter < 1000000){ // delay loop
        ++counter;
      }
      return 0;
   }
}

