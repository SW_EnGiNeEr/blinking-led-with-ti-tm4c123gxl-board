The comments in the source code explain which registers were used and show which bit values were assigned in order to make the LED blink. Pointers were used to correlate the appropriate memory addresses for each register. 

This code was later updated to allow the blinking of all three LEDs for one millisecond each. 